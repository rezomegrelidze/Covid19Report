﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Covid19Report.Services
{
    public class Covid19DataAPI
    {
        private string apiKey;

        public Covid19DataAPI()
        {
            apiKey = JObject.Parse(File.ReadAllText("secrets.json"))["apiKey"].ToString();
        }

        public IEnumerable<CountryModel> GetListOfCountries()
        {
            var client = new RestClient("https://covid-19-data.p.rapidapi.com/help/countries?format=json");
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", "covid-19-data.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", apiKey);
            IRestResponse response = client.Execute(request);
            return JArray.Parse(response.Content).Select(c => JsonConvert.DeserializeObject<CountryModel>(c.ToString()));
        }

        public CountryReport GetCountryReport(string alpha2Code)
        {
            var client = new RestClient("https://covid-19-data.p.rapidapi.com/country/code?format=json&code=it");
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", "covid-19-data.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", apiKey);
            IRestResponse response = client.Execute(request);
            return JsonConvert.DeserializeObject<CountryReport>(JArray.Parse(response.Content).First.ToString());
        }
    }

    public class CountryModel
    {
        public string Name { get; set; }
        public string Alpha2Code { get; set; }
        public string Alpha3Code { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string Flag => $"https://www.countryflags.io/{Alpha2Code}/shiny/64.png";
    }

    public class CountryReport
    {
        public string Code { get; set; }
        public string Confirmed { get; set; }
        public string Recovered { get; set; }
        public string Critical { get; set; }
        public string Deaths { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Country { get; set; }

        public string Flag => $"https://www.countryflags.io/{Code}/shiny/64.png";
    }
}