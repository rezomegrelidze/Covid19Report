﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Covid19Report.Models;
using Covid19Report.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Covid19Report.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Covid19DataAPI _covidService;

        public HomeController(ILogger<HomeController> logger,Covid19DataAPI covidService)
        {
            _logger = logger;
            _covidService = covidService;
        }

        public IActionResult Index()
        {
            var countries = _covidService.GetListOfCountries();

            return View(countries);
        }

        public IActionResult CountryReport(string alpha2Code)
        {
            var countryReport = _covidService.GetCountryReport(alpha2Code);
            return View(countryReport);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
